electron_CppAddOns (N-API)

A Native Addon in the context of the Node.js is a binary file that is compiled from a low-level language like C and C++. Hence like importing a JavaScript file using require, we would be importing a Native Addon.

Native Addon like any other .js file exposes its API on module.exports or exports object. A collection of these files when wrapped inside a node module also called as Native Module.

This Native Addon must be compatible with our JavaScript environment and must expose a compatible API so that it can be consumed like a normal node module.

An Application Binary Interface is a way for a program to communicate with other compiled programs. This is a lot similar to an API but in the case of ABI, we are interacting with a binary file and accessing memory addresses to look for symbols like numbers, objects, classes, functions, etc.

The compiled DLL file (Native Addon) when loaded into Node.js runtime actually communicates with Node.js using an ABI provided by the Node.js to access/register values and to perform tasks through APIs exposed by Node.js and its libraries.

Prerequisites
- C and C++ compilers installed on our system.
- node-gyp NPM package globally installed
- Python installed in our system

Setting up the project
- initialize the package.json file using npm init -y command. You should ignore -y flag and provide information about your package manually.
- We are going to create a Native Module module with the name greet, hence we should have greet as the value of name property inside package.json.
- install some dependencies: $ npm install -S node-addon-api
- Create Cpp and header files
- Crete the binding.gyp file as in the example

The important properties to look at in binding.gyp are target_name, sources and include_directories. Other properties are used here to ignore compiler exceptions and warnings.

The targets list contains a number of Native Addons or DLL files to be generated. At the moment, we are interested in only one DLL file. The target_name key specifies the name of this DLL file.

The sources list contains the list of .cpp files to be compiled to generate the DLL file. The include_dirs list contains the directories where the sources should look for header files if the compiler could not locate them.

Since we are going to need napi.h file soon, we need to provide its location inside include_dirs. The <!@(node -p \"require('node-addon-api').include\") command is executed by node-gyp to find the correct directory path of node-addon-api module while building the Native Addons.

The index.cpp file is going to contain some logic that will communicate with Node.js through N-API (using node-addon-api). Hence this file is also going to import napi.h header file.

If you think about typical JavaScript-based node module, it exposes its API through module.exports or exports object available to it. Similarly, our index.cpp file is going to access exports object provided by Node.js during runtime and add some symbols in exports object.

We are going to export greetHello function from our Native Addon which executes a C++ function and returns a string. This means we also need to set greetHello property on the exports object and point it to a C++ function.

- Add the index.cpp file with these steps;

    Firstly, we have imported napi.h header file provided by node-addon-api module. The resolution of the location of this file will be taken care of by node-gyp in the compilation phase using binding.gyp file.
    We have also imported <string> standard header to use string data type in our C++ code. Since this is a standard header, we do not need to worry about its resolution, the C++ compiler will take care of that.
    Also, greeting.h is needed to use declarations for symbols defined inside greeting.cpp. We have double-quotes (��) instead of angle brackets (<>) with the #include directive to signify that it will be imported from the current directory (explanation).
    The rest of the code below #include statements is standard C++ code. However, all the types, macros, and other non-standard declarations are provided by napi.h header file. (If you press Command key and hover over a declaration in VS Code, you will be able to see its implementation.)
    NODE_API_MODULE is a function (macro) provided by napi.h which wraps NAPI_MODULE macro provided by node_api.h (N-API). This function will be invoked when we import this Native Addon in a JavaScript program using require function.
    The first argument to this function is the unique name used by the Node.js while registration of the module (when we import it using require function). Ideally, it should be equal to the target_name specified inside binding.gyp file but just as a label and not as a "string" value.
    The second argument is a C++ function which is invoked when Node.js starts the registration process of the module. This function will be invoked with env and exports arguments.
    Napi:: prefix is used because declarations provided by napi.h are defined inside Napi namespace. Since macros can not be namespaced like NODE_API_MODULE, they are not used with this prefix.
    The env value is of type Napi::Env which contains information about the environment in which the registration of the module is happening. This value is needed while performing N-API operations.
    The exports object is the low-level API of module.exports object available in JavaScript. It is a type of Napi::Object which is similar to Object data type in JavaScript. We can use its Set method to add some properties to it (documentation). This function must return exports object back.
    On line no. 31, we are registering greet module and provided Init callback function. Inside Init function, we are setting greetHello property on exports object which is assigned with greetHello function.
    The property name on exports object will of Napi::String class type. However, the value of this property can be of any type like String, Number, Array, Object, Function, etc. (This is analogous to a JavaScript program where key on the exports object is a string (String type) and value can be of any valid JavaScript object).
    In the above example, we are assigning a new instance of greetHello function using Napi::Function class to the greetHello property on exports object. greetHello function receives Napi:callbackInfo object which contains arguments invoked by the function caller.
    Since we want to return a string from this function, the return type of this function is Napi::String. Using Napi::String::New, we can create a string value from UTF-8 string literal and return it.
    Inside greetHello function, we are calling helloUser C++ function which returns a UTF-8 string value. We can use this value to compose an appropriate Napi::String return value for the greetHello function.

- Generate the Native Addon (DLL) file which can be imported in Node.js as a module by $ node-gyp configure
- Execute $ node-gyp build

This command goes to the files in the build and generates a DLL file with .node extension. This file will be placed inside build/Release directory.

-If you change anything use $node-gyp rebuild
-Insert this line into WebPreferences in index.js 
	nodeIntegration: true

-Check this package to release pre-built Native Addons: https://www.npmjs.com/package/prebuild-install


Source: https://itnext.io/a-simple-guide-to-load-c-c-code-into-node-js-javascript-applications-3fcccf54fd32